<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" isELIgnored="false" %>
<html>
<head>
    <title>Exchange result</title>
</head>

<style>
    .button {
        font: bold 20px Arial;
        text-decoration: none;
        background-color: #EEEEEE;
        color: #333333;
        padding: 2px 6px 2px 6px;
        border-top: 1px solid #CCCCCC;
        border-right: 1px solid #333333;
        border-bottom: 1px solid #333333;
        border-left: 1px solid #CCCCCC;
    }
    body {
        background-image: url('https://static.vecteezy.com/system/resources/previews/001/984/880/original/abstract-colorful-geometric-overlapping-background-and-texture-free-vector.jpg');
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: cover;
    }

    .text-style {
        font-size: 20px;
        text-align:center
    }

</style>
<body>
<div class="text-style" >
<h3>The result is: </h3>
<c:choose>
    <c:when test="${isCurrencyPresent}">
        ${exchangeValue}
        <br />
    </c:when>
    <c:otherwise>
        We don't operate that currency.
        <br />
    </c:otherwise>
</c:choose>

    <br>
    <br>
<a href="/CurrencyConverter/" class="button">Go back</a>

</div>
</body>
</html>

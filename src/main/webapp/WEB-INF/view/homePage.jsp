<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" isELIgnored="false" %>
<title>Start Spring MVC</title>
</head>

<style>
    input, select {
        width: 100%;
        padding: 12px 20px;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
    }

    body {
        background-image: url('https://static.vecteezy.com/system/resources/previews/001/984/880/original/abstract-colorful-geometric-overlapping-background-and-texture-free-vector.jpg');
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: cover;
    }

    .title {
        color: white
    }

    .button-green {
        background-color: white;
        color: black;
        border: 2px solid #4CAF50;
        width: 5%;
        height: 5%
    }

</style>

<body>
<h1 class="title">Currency converter $$</h1>

<form id="value" action="exchange" method="post">
    <label>Choose currencies: </label>
    <select name="exchangeCurrencies">
        <c:forEach items="${listOfCurrencies}" var="currency">
            <option value="${currency}">${currency}</option>
        </c:forEach>
    </select>
    <br/>
    <br>
    <label>Exchange: </label>
    <br>
    <input type="number" name="amountToExchange">
    <br>
    <br>
    <button class="button-green">Submit</button>
</form>
</body>
</html>

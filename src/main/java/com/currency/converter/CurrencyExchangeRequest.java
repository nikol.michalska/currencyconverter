package com.currency.converter;

public class CurrencyExchangeRequest {
    private String exchangeCurrencies;


    private double amountToExchange;


    public String getExchangeCurrencies() {
        return exchangeCurrencies;
    }

    public void setExchangeCurrencies(String exchangeCurrencies) {
        this.exchangeCurrencies = exchangeCurrencies;
    }

    public double getAmountToExchange() {
        return amountToExchange;
    }

    public void setAmountToExchange(double amountToExchange) {
        this.amountToExchange = amountToExchange;
    }


}

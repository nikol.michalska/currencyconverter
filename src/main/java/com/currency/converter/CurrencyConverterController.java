package com.currency.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CurrencyConverterController {

    private final CurrencyConverterService converterService;

    @Autowired
    public CurrencyConverterController(CurrencyConverterService converterService) {
        this.converterService = converterService;
    }

    @GetMapping(value = "/")
    public ModelAndView firstView() {
        ModelAndView mav = new ModelAndView("homePage");
        mav.addObject("listOfCurrencies", converterService.exchangeRate.keySet());
        return mav;
    }

    @PostMapping(value = "/exchange")
    public ModelAndView exchangeCurrency(CurrencyExchangeRequest converter) {
        boolean isCurrencyPresent = converterService.isCurrencyPresent(converter);
        ModelAndView mav = new ModelAndView("result");
        if(isCurrencyPresent){
            double exchangeValue = converterService.exchangeValue(converter);
            mav.addObject("exchangeValue", exchangeValue);
        }

        mav.addObject("isCurrencyPresent", isCurrencyPresent);
        return mav;
    }
}

package com.currency.converter;

import org.springframework.stereotype.Service;

import java.util.HashMap;


@Service
public class CurrencyConverterService {

    public static HashMap<String, Double> exchangeRate = new HashMap<String, Double>() {{
        put("PLN->EUR", 0.8);
        put("EUR->PLN", 4.5);
        put("PLN->GBP", 0.1);
        put("GBP->PLN", 5.2);
        put("PLN->USD", 0.2);
        put("USD->PLN", 4.1);
    }};

    public double exchangeValue(CurrencyExchangeRequest exchangeRequest) {
        double exchangeValue = exchangeRate.get(exchangeRequest.getExchangeCurrencies());
        return exchangeRequest.getAmountToExchange() * exchangeValue;
    }

    public boolean isCurrencyPresent(CurrencyExchangeRequest exchangeRequest) {
        return exchangeRate.containsKey(exchangeRequest.getExchangeCurrencies());
    }
}
